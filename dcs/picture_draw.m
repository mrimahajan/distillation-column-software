syms X i j N A B AB BB h
h=10;                            %hieght of column
i=1;                             %no. of rows
j=1;                             %no. of column
N=input('Enter the order of matrix');
X=input('Enter the matrix');
axis([-10 10 -20 20]);           %graph size
plot([-9.8 -9],[0 0],'r-');
d = zeros(1,N+1-j);
for p=1:N+1-j
d(p)= 64+i+p-1;
end
text(-9.5,0.5,(char(d)));
A=[-8 -7];                     % A & B are the top stream co-ordinate
BB=[-10 -10];
AB=[-8 -7];                    %AB & BB are the bottam co-ordinate
B=[10 10];
hold on
rectangle('position',[-9 -10 1 20]);
plot(A,B,'r-');
plot(AB,BB,'r-');
UPPER_STREAM(i,j,X,A,B,h,N);                       %Upper product
LOWER_STREAM(i,j,X,AB,BB,h,N);                    %Lower product
hold off