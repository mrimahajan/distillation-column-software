function LOWER_STREAM(i,j,X,AB,BB,h,N)
syms A2 B2  BB1 AB2 BB2
if((j==N)&&(i==N))          %when all matrix index complete
    return
else
    i=i+1;
    j=j+1;
    while((i<N)&&(j<N))
       if( X(i,j)==1)
           h=h-2;
            d = zeros(1,N+1-j);
            for p=1:N+1-j
               d(p)= 64+i+p-1;
            end
             text((AB(1)+AB(2))/2,BB(1)+0.5,(char(d)));
           rectangle('position',[AB(2) (BB(2)-h/2) 0.5 h]);
            A2=[AB(2)+0.5 AB(2)+1.5];
            B2=[BB(2)+h/2 BB(2)+h/2];
            AB2=[AB(2)+0.5 AB(2)+1.5];
            BB2=[BB(2)-h/2 BB(2)-h/2];
            plot(A2,B2,'r-');
            plot(AB2,BB2,'r-');
            UPPER_STREAM(i,j,X,A2,B2,h,N);
            LOWER_STREAM(i,j,X,AB2,BB2,h,N);
            break;
       else
           i=i+1;
           j=j+1;
           continue;
       end
    end
end
if(j==N)
    d = zeros(1,N+1-j);
    for p=1:N+1-j
        d(p)= 64+i+p-1;
    end
    text((AB(1)+AB(2))/2,BB(1)+0.5,(char(d)));
end
return


end

