function UPPER_STREAM(i,j,X,A,B,h,N)
syms A1 B1 AB1 BB1 l
if((j==N)&&(i==N))                                                    %when all matrix index complete
    return                                                   %check this is correct or not
else
    j=j+1;
    while(j<N)
        if( X(i,j)==1)
            d = zeros(1,N+1-j);
            for p=1:N+1-j
               d(p)= 64+i+p-1;
            end
             text((A(1)+A(2))/2,B(1)+0.5,(char(d)));
            h=h-2;               %each distillation column has hieght less than 2 from previous one
            L=(B(2)-h/2);
           rectangle('position',[A(2) L 0.5 h]);
           A1=[A(2)+0.5 A(2)+1.5];                                   
           B1=[B(2)+h/2 B(2)+h/2];
           AB1=[A(2)+0.5 A(2)+1.5];
           BB1=[B(2)-h/2 B(2)-h/2];
           plot(A1,B1,'r-');
           plot(AB1,BB1,'r-');
           UPPER_STREAM(i,j,X,A1,B1,h,N);
           LOWER_STREAM(i,j,X,AB1,BB1,h,N);
           break;
        else
            j=j+1;
            continue;
        end
    end
   
end
if(j==N)
    d = zeros(1,N+1-j);
    for p=1:N+1-j
        d(p)= 64+i+p-1;
    end
    text((A(1)+A(2))/2,B(1)+0.5,(char(d)));
end
return

end

